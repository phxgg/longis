[![Codacy Badge](https://app.codacy.com/project/badge/Grade/78bb2b037cd744179714741ab9ddfd01)](https://app.codacy.com/gl/longis/longis/dashboard?utm_source=gl&utm_medium=referral&utm_content=&utm_campaign=Badge_grade)

# @longis/longis

Universis project extensions for Longis ecosystem

## Installation

    npm i @longis/longis

## Usage

Register `LongisService` and `LongisSchemaReplacer` under application services:

    # app.production.json

    "services": [
        ...,
        {
            "serviceType": "@longis/longis#LongisSchemaReplacer"
        },
        {
            "serviceType": "@longis/longis#LongisService"
        }
    ]

Add `LongisSchemaLoader` to schema loaders

    # app.production.json
    
    {
        "settings": {
            "schema": {
                "loaders": [
                    ...,
                    {
                        "loaderType": "@longis/longis#LongisSchemaLoader"
                    }
                ]
            }
        }
    }


