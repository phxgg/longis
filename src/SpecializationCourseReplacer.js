import { ApplicationService } from '@themost/common';
import { SchemaLoaderStrategy } from '@themost/data';
import path from 'path';

class SpecializationCourseReplacer extends ApplicationService {
  constructor(app) {
    super(app);
  }

  apply() {
    // get schema loader
    const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
    // get model definition
    const model = schemaLoader.getModelDefinition('SpecializationCourse');

    if (model) {
      model.eventListeners = model.eventListeners || [];
      const insertIndex = model.eventListeners.findIndex((item) => item.type === '@themost/data/previous-state-listener');
      const insertListener =  {
        type: path.resolve(__dirname, 'listeners/auto-create-course-listener')
      };
      model.eventListeners.splice(insertIndex + 1, 0, insertListener);
      schemaLoader.setModelDefinition(model);
    }
  }

}

export {
  SpecializationCourseReplacer
}
