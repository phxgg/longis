import { ApplicationService, TraceUtils } from '@themost/common';
import { SchemaLoaderStrategy } from '@themost/data';

class TimeTableEventReplacer extends ApplicationService {
    constructor(app) {
        super(app);
    }

    apply() {
        try {
            // get schema loader
            const schemaLoader = this.getApplication()
                .getConfiguration()
                .getStrategy(SchemaLoaderStrategy);
            // get model definition
            const model = schemaLoader.getModelDefinition('TimetableEvent');
            const findAttribute = model.fields.find((field) => {
                return field.name === 'studyPrograms';
            });
            if (findAttribute == null) {
                model.fields.push({
                    // new added field in model with name, title, description and type
                    name: 'studyPrograms',
                    title: 'Timetable StudyPrograms',
                    description: 'A collection of studyPrograms where a timeTable has.',
                    type: 'StudyProgram',
                    many: true,
                    mapping: {
                        // juction as associationType, wil create a new table
                        associationType: 'junction', 
                        // new table's name 
                        associationAdapter: 'TimeTableStudyPrograms',
                        // object for field parentId in new table
                        parentModel: 'StudyProgram', 
                        parentField: 'id',
                        // object for field valueId in new table
                        childModel: 'TimetableEvent',
                        childField: 'id',
                        cascade: 'delete',   
                    }
                });
            }

            schemaLoader.setModelDefinition(model);
            
        } catch (err) {
            TraceUtils.error(
                'An error occured while the LongisService tried to extend the TimeTable model (TimeTableEventReplacer).'
            );
            TraceUtils.error(err);
            throw err;
        }
    }
}

export {
    TimeTableEventReplacer
  }
