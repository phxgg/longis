import { ApplicationService, TraceUtils } from '@themost/common';
import { EdmType, EdmMapping, SchemaLoaderStrategy, ModelClassLoaderStrategy, DataObject } from '@themost/data';
import path from 'path';
import { QueryExpression, QueryField, QueryEntity } from '@themost/query';

class StudyProgram extends DataObject {

  @EdmMapping.func('events', EdmType.CollectionOf('Event'))
  async getEvents() {
    try {
      /**
       * @type {import('@themost/data').DataQueryable}
       */
      const q = this.context.model('Event').asQueryable();
      q.select();
      const Events = new QueryEntity(q.query.$collection);
      // ensure that the association between TimeTableEvent and StudyProgram has been already created
      // try to upgrade TimetableEvent
      await this.context.model('TimetableEvent').migrateAsync();
      // try to upgrade TimeTableStudyPrograms which is the model that holds the association between
      // TimetableEvent and StudyProgram
      /**
       * @type {import('@themost/data').DataModel}
       */
      const TimeTableStudyPrograms = this.context.model('TimetableEvent').convert({ id:0 }).property('studyPrograms').getBaseModel();
      await TimeTableStudyPrograms.migrateAsync();
      const StudyProgramTimeTableEvents = new QueryEntity(TimeTableStudyPrograms.sourceAdapter).as('StudyProgramTimeTableEvents');
      // pseudo-sql: INNER JOIN TimeTableStudyPrograms AS StudyProgramTimeTableEvents 
      // ON StudyProgramTimeTableEvents.parentId = <id>
      const parent = TimeTableStudyPrograms.fields.find((x) => x.type === 'StudyProgram');
      q.query.join(StudyProgramTimeTableEvents).with(
        new QueryExpression().where(new QueryField(parent.name).from(StudyProgramTimeTableEvents)).equal(
          this.getId()
        )
      );
      const child = TimeTableStudyPrograms.fields.find((x) => x.type === 'TimetableEvent');
      const TimeTableEvents = new QueryEntity(this.context.model('TimetableEvent').sourceAdapter).as('TimeTableEvents0');
      // pseudo-sql: INNER JOIN TimeTableEvents AS TimeTableEvents0 
      // ON StudyProgramTimeTableEvents.valueId = TimeTableEvents.id
      // AND Events.superEvent = TimeTableEvents0.id
      q.query.join(TimeTableEvents).with(
        new QueryExpression().where(new QueryField(child.name).from(StudyProgramTimeTableEvents)).equal(
          new QueryField('id').from(TimeTableEvents)
        ).and(
          new QueryField('superEvent').from(Events)
        ).equal(
          new QueryField('id').from(TimeTableEvents)
        )
      );
      return q;
    } catch (err) {
      TraceUtils.error(err);
      throw err;
    }
  }

  @EdmMapping.func('classes', EdmType.CollectionOf('CourseClass'))
  async getCourseClasses() {
    try {
      /**
       * @type {import('@themost/data').DataQueryable}
       */
      const q = this.context.model('CourseClass').asQueryable();
      q.select();
      // pseudo-sql INNER JOIN StudyProgramCourseData ON StudyProgramCourseData.studyProgram = 100
      const StudyProgramCourses = new QueryEntity(this.context.model('StudyProgramCourse').sourceAdapter).as('StudyProgramCourses0')
      q.query.join(StudyProgramCourses).with(
        new QueryExpression().where(new QueryField('studyProgram').from(StudyProgramCourses)).equal(
          this.getId()
        )
      );
      // pseudo-sql INNER JOIN CourseData ON StudyProgramCourseData.course = CourseData.id AND CourseData.id = CourseClass.course
      const Courses = new QueryEntity(this.context.model('Course').sourceAdapter).as('Courses0')
      q.query.join(Courses).with(
        new QueryExpression().where(new QueryField('course').from(StudyProgramCourses)).equal(
          new QueryField('id').from(Courses)
        ).and(
          new QueryField('course').from(q.query.$collection)
        )
          .equal(
            new QueryField('id').from(Courses)
          )
      )
      return q;
    } catch (err) {
      TraceUtils.error(err);
      throw err;
    }
  }

}

class StudyProgramReplacer extends ApplicationService {
  constructor(app) {
    super(app);
  }

  apply() {
    // get schema loader
    const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
    // get model definition
    const model = schemaLoader.getModelDefinition('StudyProgram');
    if (model) {
      model.eventListeners = model.eventListeners || [];
      const insertIndex = model.eventListeners.findIndex((item) => item.type === '@themost/data/previous-state-listener');
      const insertListener = {
        type: path.resolve(__dirname, 'listeners/after-save-create-timetable-listener')
      };
      model.eventListeners.splice(insertIndex + 1, 0, insertListener);
      schemaLoader.setModelDefinition(model);

      // get model class
      const loader = this.getApplication().getConfiguration().getStrategy(ModelClassLoaderStrategy);
      const StudyProgramClass = loader.resolve(model);
      StudyProgramClass.prototype.getCourseClasses = StudyProgram.prototype.getCourseClasses;
      StudyProgramClass.prototype.getEvents = StudyProgram.prototype.getEvents;

    }
  }

}

export {
  StudyProgramReplacer
}
